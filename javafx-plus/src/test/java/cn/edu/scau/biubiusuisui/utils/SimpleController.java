package cn.edu.scau.biubiusuisui.utils;

/**
 * @author Jade Yeung
 * @time 2022/5/3 20:29
 * @since 1.3.0
 */
public class SimpleController {
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
